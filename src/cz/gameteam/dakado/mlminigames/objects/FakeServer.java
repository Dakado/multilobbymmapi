package cz.gameteam.dakado.mlminigames.objects;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;

import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class FakeServer implements ServerInfo {
	
	
	private String name;
	
	public FakeServer(String name) {
		
	}

	@Override
	public boolean canAccess(CommandSender arg0) {
		return true;
	}

	@Override
	public InetSocketAddress getAddress() {
		return null;
	}

	@Override
	public String getMotd() {
		return "Online";
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Collection<ProxiedPlayer> getPlayers() {
		return new ArrayList();
	}

	@Override
	public void ping(Callback<ServerPing> arg0) {
		
		
	}

	@Override
	public void sendData(String arg0, byte[] arg1) {
		
		
	}

	@Override
	public boolean sendData(String arg0, byte[] arg1, boolean arg2) {
		return true;
	}

}
