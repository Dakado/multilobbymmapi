package cz.gameteam.dakado.mlminigames.objects;

import java.util.ArrayList;
import java.util.List;

public class Group {
	
	private String name;
	private List<String> joinStatus = new ArrayList<String>();
	private List<String> servers = new ArrayList<String>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getServers() {
		return servers;
	}
	public void setServers(List<String> servers) {
		this.servers = servers;
	}
	public List<String> getJoinStatus() {
		return joinStatus;
	}
	public void setJoinStatus(List<String> joinStatus) {
		this.joinStatus = joinStatus;
	}
	
	/**
	 * Is this group joinable
	 * @param status
	 * @return
	 */
	public boolean isJoinable(String status) {
		return joinStatus.contains(status);
	}

}
