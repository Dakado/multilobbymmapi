package cz.gameteam.dakado.mlminigames;

import cz.gameteam.dakado.mlminigames.objects.Group;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public interface MatchMaker {
	
	
	/**
	 * Send player to a specific group
	 * @param pp
	 * @param group
	 */
	public abstract void joinGroup(ProxiedPlayer pp, String group);
	
	/**
	 * Check if the player has MM immunity
	 * @param player
	 * @return
	 */
	public abstract boolean isImmune(String player);
	
	/**
	 * Add MatchMaker immunity to a given player
	 * @param player
	 */
	public abstract void addImmunity(String player);
	
	
	/**
	 * Get the group of the server, if not exists returns null
	 * @param serverName
	 * @return
	 */
	public abstract Group getServerGroup(String serverName);
	
	/**
	 * Get group by string name
	 * @param group
	 * @return
	 */
	public abstract Group getGroupByName(String group);
	
	/**
	 * Get joinable/free server from the given group
	 * @param group
	 * @return null if no free server found
	 */
	public abstract String getJoinableServer(String group);
	
	/**
	 * Get joinable server but request at least given number of slots so all party can fit in
	 * @param group
	 * @param minSlots
	 * @return
	 */
	public abstract String getJoinableServer(String group, int minSlots);
	
	/**
	 * Get total joinable servers number (for statistical purposes only)
	 * @param group
	 * @return
	 */
	public abstract int getTotalJoinableServers(String group);
	
	/**
	 * Check if all servers in the given group are full, in that case we need to put player in the queue
	 * @param group
	 * @return
	 */
	public abstract boolean isGroupFull(String group);

}
