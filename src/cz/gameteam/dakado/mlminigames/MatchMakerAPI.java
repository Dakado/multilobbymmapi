package cz.gameteam.dakado.mlminigames;

import cz.gameteam.dakado.mlminigames.objects.Group;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class MatchMakerAPI implements MatchMaker {
	
	public static MatchMakerAPI api;
	
	public static MatchMakerAPI getInstance() {
		if (api == null) api = new MatchMakerAPI();
		return api;
	}
	
	/**
	 * Send player to a specific group
	 * @param pp
	 * @param group
	 */
	public void joinGroup(ProxiedPlayer pp, String group) {

	}
	
	/**
	 * Check if the player has MM immunity
	 * @param player
	 * @return
	 */
	public boolean isImmune(String player) {
		return false;
	}
	
	/**
	 * Add MatchMaker immunity to a given player
	 * @param player
	 */
	public void addImmunity(String player) {
		
	}
	
	
	/**
	 * Get the group of the server, if not exists returns null
	 * @param serverName
	 * @return
	 */
	public Group getServerGroup(String serverName) {
		return null;
	}
	
	/**
	 * Get group by string name
	 * @param group
	 * @return
	 */
	public Group getGroupByName(String group) {
		return null;
	}
	
	/**
	 * Get joinable/free server from the given group
	 * @param group
	 * @return null if no free server found
	 */
	public String getJoinableServer(String group) {
		return null;
	}
	
	/**
	 * Get joinable server but request at least given number of slots so all party can fit in
	 * @param group
	 * @param minSlots
	 * @return
	 */
	public String getJoinableServer(String group, int minSlots) {
		return null;
	}
	
	/**
	 * Get total joinable servers number (for statistical purposes only)
	 * @param group
	 * @return
	 */
	public int getTotalJoinableServers(String group) {
		return 0;
	}
	
	/**
	 * Check if all servers in the given group are full, in that case we need to put player in the queue
	 * @param group
	 * @return
	 */
	public boolean isGroupFull(String group) {
		return false;
	}

}
